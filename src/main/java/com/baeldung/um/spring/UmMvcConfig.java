package com.baeldung.um.spring;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@Configuration
@EnableWebMvc
//@ComponentScan("com.baeldung.um")
public class UmMvcConfig extends WebMvcConfigurerAdapter {
	
	
    

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("loginPage");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/home").setViewName("home");
//        registry.addViewController("/").setViewName("home");
//        registry.addViewController("/dashboard").setViewName("dashboard");
//        registry.addViewController("/login").setViewName("login");
//    }
    // thymeleaf

    @Bean
    public ClassLoaderTemplateResolver templateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("templates/");
        templateResolver.setSuffix(".html");
        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        return templateEngine;
    }

    @Bean
    public ThymeleafViewResolver viewResolver() {
        final ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        return viewResolver;
    }

    // end thymeleaf

    @Configuration
    @EnableOAuth2Client
    protected static class OAuthClientConfig {
    	@Value("${security.oauth2.client.id}")
        private String clientID;

        @Value("${security.oauth2.client.client-secret}")
        private String clientSecret;
        
    	@Value("${security.oauth2.client.access-token-uri}")
        private String accessTokenUri;

        @Value("${security.oauth2.client.user-authorization-uri}")
        private String userAuthorizationUri;

        @Value("${security.oauth2.client.auto-approve-scopes}")
        private List<String> scope;

        @Value("${security.oauth2.client.grant-type}")
        private String grantType;
        
        @Value("${security.oauth2.client.use-current-uri}")
        private Boolean useCurrentUri;

        @Value("${security.oauth2.resource.user-info-uri}")
        private String resouceUserInfoUri;
        
        @Bean
        public OAuth2ProtectedResourceDetails resourceDetails() {
            
        	final CustomAuthorizationCodeResourceDetails details = new CustomAuthorizationCodeResourceDetails(); 	
//        	final AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
            details.setClientId(clientID);
            details.setClientSecret(clientSecret);
            details.setAccessTokenUri(accessTokenUri);
            details.setUserAuthorizationUri(userAuthorizationUri);
            details.setScope(scope);
            details.setGrantType(grantType);
            details.setUseCurrentUri(useCurrentUri);
//            details.setUserinfoUri(resouceUserInfoUri);
            return details;
        }

        @Bean
        public OAuth2RestTemplate redditRestTemplate(final OAuth2ClientContext clientContext) {
            return new OAuth2RestTemplate(resourceDetails(), clientContext);
        }
    }

}