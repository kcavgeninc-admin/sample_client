package com.baeldung.um.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.baeldung.um.web.model.User;

@Controller
@RequestMapping("/user")
class MainController {

    @Autowired
    private OAuth2RestTemplate restTemplate;

    //

    @RequestMapping("/getUserAK")
	public String getUserAcknowledgement() {
    	final String userak = restTemplate.getForObject("http://localhost:8083/webapp-resource-server/api/users", String.class);
		System.out.println(userak);
		return userak;
	}
    @GetMapping("/greeting")
    public ModelAndView greeting(@RequestParam(name="name", required=false, defaultValue="World") String name) {
    	final String userak = restTemplate.getForObject("http://localhost:8083/webapp-resource-server/api/greeting", String.class);
		System.out.println(userak);
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.addObject(userak, name);
    	modelAndView.setViewName("greeting");
        return modelAndView;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView list() {
        final List<User> users = restTemplate.getForObject("http://localhost:8083/webapp-resource-server/api/users", List.class);
//    	final List<User> users = restTemplate.getForObject("http://localhost:8083/um-webapp-resource-server/api/users", List.class);
        return new ModelAndView("list", "users", users);
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(User user) {
        final MultiValueMap<String, String> param = new LinkedMultiValueMap<String, String>();
        param.add("email", user.getEmail());
        param.add("password", user.getPassword());
        final User created = restTemplate.postForObject("http://localhost:8083/webapp-resource-server/api/users", param, User.class);
//        final User created = restTemplate.postForObject("http://localhost:8083/um-webapp-resource-server/api/users", param, User.class);
        System.out.println(created);
        return "redirect:/user";
    }

    @RequestMapping(params = "form", method = RequestMethod.GET)
    public String createForm(@ModelAttribute final User user) {
        return "form";
    }
    
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashboard() {
        ModelAndView modelAndView = new ModelAndView();
        final User user = restTemplate.getForObject("http://localhost:8083/webapp-resource-server/api/users", User.class);
        modelAndView.addObject("currentUser", user);
        modelAndView.addObject("fullName", "Welcome " + user.getFullname());
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
        modelAndView.setViewName("dashboard");
        return modelAndView;
    }
    
    //Just send back login html page. No redirection needed
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public ModelAndView login() {
//    	final User user = restTemplate.getForObject("http://localhost:8082/um-webapp-auth-server/api/users", User.class);
//        return modelAndView;
//    }
    
    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public ModelAndView signup() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("signup");
        return modelAndView;
    }

}
